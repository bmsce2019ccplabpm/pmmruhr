#include<stdio.h>
 struct dob
    {
        int day;
        int month;
        int year;
    };
    struct employee
    {
        int id;
        char name[50];
        struct dob d;
        float salary;
    };
    int main()
    {
        struct employee e;
        printf("Enter the id of Employee\n");
        scanf("%d",&e.id);
        printf("Enter the name of Employee\n");
        scanf("%s",e.name);
        printf("Enter the date of birth of Employee\n");
        scanf("%d%d%d",&e.d.day,&e.d.month,&e.d.year);
        printf("Enter the salary of Employee\n");
        scanf("%f",&e.salary);
        printf("The id of employee is: %d\n",e.id);
        printf("The name of the employee is: %s\n",e.name);
        printf("The date of birth of the employee is: %d-%d-%d\n",e.d.day,e.d.month,e.d.year);
        printf("The salary of the employee is: %.2f",e.salary);
        return 0;
        }