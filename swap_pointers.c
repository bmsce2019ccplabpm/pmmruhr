#include<stdio.h>
int input()
{
    int a;
    printf("Enter the number: \n");
    scanf("%d",&a);
    return a;
}
void swap(int* x,int* y)
{
    int t;
    t=*x;
    *x=*y;
    *y=t;
}
void output(int *w, int *z)
{
    printf("The numbers after swap is: a=%d and b=%d\n",*w,*z);
}
int main()
{
    int a,b;
    a=input();
    b=input();
    swap(&a,&b);
    output(&a,&b);
    return 0;
}