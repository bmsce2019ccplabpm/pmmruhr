#include<stdio.h>
#include<math.h>
int main()
{
    int a,b,c;
    float d,deno,root1,root2;
    printf("Enter The value of a, b and c \n");
    scanf("%f%f%f",&a,&b,&c);
    d=(b*b)-(4*a*c);
    deno=2*a;
    if(d>0)
    {
        printf("REAL ROOTS \n");
        root1=(-b+sqrt(d))/deno;
        root2=(-b-sqrt(d))/deno;
        printf("The roots for the given number is %f and %f \n",root1,root2);
    }
    else if (d==0)
    {
        printf("EQUAL ROOTS \n");
        root1=-b/deno;
        printf("The root for the given number is %f",root1);
    }
    else
    printf("IMAGINARY ROOTS\n");
    return 0;
}
